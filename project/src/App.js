import React from 'react';
import logo from './logo.svg';
import './App.css';
import Button from '@material-ui/core/Button';
import Album from './components/Album.js';

function Componente(props){
  const value = 2;
  const {index, ... rest } = props;

  return (
  <h2>Exemplo {value}</h2>
  )
}

function App() {
  return (
    <div>
      <Album />
    </div>
  );
}

export default App;
